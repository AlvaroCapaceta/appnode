import express from 'express';
import ejs from 'ejs';
import path from 'path';
import { fileURLToPath } from 'url';
import cors from 'cors';

// Routes
import misRutas from './router/index.js';
import routeAlumno from './router/alumno.routes.js';

const __dirname = path.dirname(fileURLToPath(import.meta.url));
const app = express();

// Views
app.set('view engine', 'ejs');
app.use(express.urlencoded({ extended: true }));
app.use(express.json());

// Cambiar extensiones HTML a EJS
app.engine('html', ejs.renderFile);

app.use(cors());
app.use(misRutas.router);
app.use(routeAlumno.router);
app.use(express.static(__dirname + '/public'));

// La pagina de error va siempre al final de get/post
app.use((req, res, next) => {
  res.status(404).sendFile(__dirname + '/public/error.html');
});

const port = 3000;
app.listen(port, () => {
  console.log(`Server on port: ${port}`);
});
