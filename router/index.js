import express from 'express';
export const router = express.Router();

// Declare objects array

let datos = [
  {
    matricula: '2019030309',
    nombre: 'ACOSTA ORTEGA JESUS HUMBERTO',
    sexo: 'M',
    materias: ['Ingles', 'Base de Datos', 'Tecnología I'],
  },
  {
    matricula: '2020030310',
    nombre: 'ACOSTA VARELA IRVING GUADALUPE',
    sexo: 'M',
    materias: ['Ingles', 'Base de Datos', 'Tecnología I'],
  },
  {
    matricula: '2020030817',
    nombre: 'ALMOGABAR VAZQUEZ YARLEN DE JESUS',
    sexo: 'F',
    materias: ['Ingles', 'Base de Datos', 'Tecnología I'],
  },
  {
    matricula: '2020030170',
    nombre: 'CAPACETA MAGALLANES ALVARO ANTONIO',
    sexo: 'M',
    materias: ['Ingles', 'Base de Datos', 'Tecnología I'],
  },
];

router.get('/', (req, res) => {
  res.render('index.html', {
    titulo: 'Mi Primer Pagina en Embedded Javascript',
    name: 'Alvaro Antonio Capaceta Magallanes',
    group: '8-3',
    listado: datos,
  });
});

router.get('/tabla', (req, res) => {
  const params = {
    numero: req.query.numero,
  };

  res.render('tabla.html', params);
});

router.post('/tabla', (req, res) => {
  const params = {
    numero: req.body.numero,
  };

  res.render('tabla.html', params);
});

router.get('/cotizacion', (req, res) => {
  const params = {
    valor: req.query.valor,
    pinicial: req.query.pinicial,
    plazo: req.query.plazo,
  };

  res.render('cotizacion.html', params);
});

router.post('/cotizacion', (req, res) => {
  const params = {
    valor: req.body.valor,
    pinicial: req.body.pinicial,
    plazo: req.body.plazo,
  };

  res.render('cotizacion.html', params);
});

export default { router };
