import mysql from 'mysql2';

export const sqlConnection = mysql.createConnection({
  host: 'localhost',
  user: 'root',
  password: 'helloworld',
  database: 'sistema',
});

sqlConnection.connect((err) => {
  if (err) {
    return console.log(`Error on trying connect to database: ${err}`);
  }
  console.log('Database connected successfully...');
});

export default { sqlConnection };
